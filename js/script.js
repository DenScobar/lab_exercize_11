/* Задания на урок:

1) Удалить все рекламные блоки со страницы (правая часть сайта)

2) Изменить жанр фильма, поменять "комедия" на "драма"

3) Изменить задний фон постера с фильмом на изображение "bg.jpg". Оно лежит в папке img.
Реализовать только при помощи JS

4) Список фильмов на странице сформировать на основании данных из этого JS файла.
Отсортировать их по алфавиту 

5) Добавить нумерацию выведенных фильмов */

'use strict';

const movieDB = {
    movies: [
        "Логан",
        "Лига справедливости",
        "Ла-ла лэнд",
        "Одержимость",
        "Скотт Пилигрим против..."
    ]
};

// Обращение к селекторам
const adv = document.querySelector('.promo__adv'),
    content = document.querySelector('.promo__content'),
    bg = content.querySelector('.promo__bg'),
    genre = bg.querySelector('.promo__genre'),
    films = content.querySelectorAll('.promo__interactive-item');


// 1) Удалить все рекламные блоки со страницы (правая часть сайта)
adv.remove();
content.style.width = 'calc(100% - 300px)';

// 2) Изменить жанр фильма, поменять "комедия" на "драма"
genre.innerHTML = 'Драма';

// 3) Изменить задний фон постера с фильмом на изображение "bg.jpg". Оно лежит в папке img.
// Реализовать только при помощи JS
bg.style.backgroundImage = 'url(img/bg.jpg)';

// 4) Список фильмов на странице сформировать на основании данных из этого JS файла.
// Отсортировать их по алфавиту 
const films_sorted = movieDB.movies.sort();
films.forEach((item, index) => {
    item.innerHTML = `${films_sorted[index]}<div class="delete"></div>`;
})

// 5) Добавить нумерацию выведенных фильмов
films.forEach((item, index) => {
    item.innerHTML = `${index + 1}. ${films_sorted[index]}<div class="delete"></div>`;
})
